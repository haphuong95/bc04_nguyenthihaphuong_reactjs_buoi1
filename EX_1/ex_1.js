function ketQua() {
  var diemChuan = document.getElementById("diem-chuan").value * 1;
  var diemMon1 = document.getElementById("diem-1").value * 1;
  var diemMon2 = document.getElementById("diem-2").value * 1;
  var diemMon3 = document.getElementById("diem-3").value * 1;
  var khuVuc = document.getElementById("khu-vuc").value;
  var doiTuong = document.getElementById("doi-tuong").value;

  // tính điểm ưu tiên khu vực
  var diemKhuVuc;
  switch (khuVuc) {
    case "A": {
      diemKhuVuc = 2;
      break;
    }
    case "B": {
      diemKhuVuc = 1;
      break;
    }
    case "C": {
      diemKhuVuc = 0.5;
      break;
    }
    case "X": {
      diemKhuVuc = 0;
      break;
    }
    default: {
      diemKhuVuc = `Bạn chưa chọn khu vực để tính điểm ưu tiên. `;
    }
  }

  // tính điểm ưu tiên đối tượng
  var diemDoiTuong;
  switch (doiTuong) {
    case "1": {
      diemDoiTuong = 2.5;
      break;
    }
    case "2": {
      diemDoiTuong = 1.5;
      break;
    }
    case "3": {
      diemDoiTuong = 1;
      break;
    }
    case "0": {
      diemDoiTuong = 0;
      break;
    }
    default: {
      diemDoiTuong = `Bạn chưa chọn đối tượng để tính điểm ưu tiên. `;
    }
  }

  // tính điểm tổng
  var diemTong = 0;
  diemTong = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;

  // xử lí kết quả
  var result;
  if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    result = `RỚT do có môn 0 điểm. Tổng điểm là: ${diemTong}`;
  } else if (diemTong >= diemChuan) {
    result = `ĐẬU. Tổng điểm là: ${diemTong}`;
  } else {
    result = `RỚT do tổng điểm thấp hơn điểm chuẩn. Tổng điểm là: ${diemTong}`;
  }

  // in kết quả
  document.getElementById("result").innerHTML = `<h3 class="py-4">
  Điểm chuẩn: ${diemChuan} <br/>
  Điểm môn thứ nhất: ${diemMon1} <br/>
  Điểm môn thứ hai: ${diemMon2} <br/>
  Điểm môn thứ ba: ${diemMon3} <br/>
  Điểm ưu tiên khu vực: ${diemKhuVuc} <br/>
  Điểm ưu tiên đối tượng: ${diemDoiTuong} <br/>
  Kết quả: Bạn đã ${result}
  </h3>`;
}
